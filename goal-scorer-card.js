import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

import '@polymer/paper-styles/paper-styles.js';
import '@polymer/iron-image/iron-image.js';

/**
 * `goal-scorer-card`
 * Details of player about goal scorer in a tournament
 *
 * @customElement
 * @polymer
 * @demo demo/index.html
 */
class GoalScorerCard extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
        
        .horizontal {
          @apply --layout-horizontal;
        }
        
        .vertical {
          @apply --layout-vertical;
        }
        
        .justified {
          @apply --layout-justified;
        }
        
        .start {
          @apply --layout-start-justified;
        }
        
        .center {
          @apply --layout-center-justified;
        }
        
        .position {
          @apply --paper-font-display3;
          color: var(--paper-grey-700);
        }
        
        .goals {
          @apply --paper-font-display4;
          color: var(--paper-grey-900);
        }
        
        .name {
          @apply --paper-font-title;
          color: var(--paper-grey-800);
        }
        
        .data {
          @apply --paper-font-subhead;
          color: var(--paper-grey-600);
        }
        
        iron-image {
          width: 100px;
          height: 100px;
        }
        
        .vr {
          border-left: 1px solid #E0E0E0;
        }
      </style>
      
      <div class="horizontal justified">
        <div class="vertical center">
          <span class="position">[[place]]</span>
        </div>
        <div class="vertical center">
          <iron-image src="[[image]]" sizing="cover"></iron-image>
        </div>
        <div class="vertical center">
          <div class="vertical start">
            <span class="name">[[name]]</span>
            <span class="data">[[team]]</span>
            <span class="data">[[position]]</span>
          </div>
        </div>
        <div class="vr"></div>
        <div class="vertical center horizontal">
          <span class="goals">[[goals]]</span>
        </div>
      </div>
    `;
  }
  static get properties() {
    return {
      place: {
        type: Number,
        value: 0
      },
      image: {
        type: String,
        value: 'https://i.imgur.com/IYlX60y.png'
      },
      name: {
        type: String,
        value: 'Nombre Apellido P'
      },
      team: {
        type: String,
        value: 'Equipo 1'
      },
      position: {
        type: String,
        value: 'POS'
      },
      goals: {
        type: Number,
        value: 0
      }
    };
  }
}

window.customElements.define('goal-scorer-card', GoalScorerCard);
